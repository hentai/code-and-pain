'use strict';

const passport = require('passport');
const Strategy = require('passport-local');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('./config');
const db = require('../app/models');
const jdenticon = require('jdenticon');
const fs = require('fs');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const googleAuthConfig = require('./google-auth.json');
const GitHubStrategy = require('passport-github2').Strategy;
const gitHubAuthConfig = require('./github-auth.json');
const FacebookStrategy = require('passport-facebook').Strategy;
const facebookAuthConfig = require('./facebook-auth.json');

module.exports = function (app) {
    app.use(passport.initialize());

    passport.use(new Strategy(
        (email, password, cb) => {
            db.User.findOne({where: {email}}).then(user => {
                if (!user) {
                    return cb(null, false);
                }
                return bcrypt.compare(password, user.password, (err, res) => {
                    if (!res) {
                        return cb(null, false);
                    }
                    return cb(null, user);
                });
            });
        }));

    /**
     * Gets POST request for registering new user
     * Sends one of the following responses:
     *  - 200 (OK) - New user has successfully added to the database (registered)
     *  - 409 (Bad Request) - User already exists in the database
     *  - 400 (Bad Request) - Credentials are not valid or database is not available
     */
    app.post('/register',
        (req, res) => {
            const saltRounds = 10;
            const user = {};
            const pathToAvatars = 'public/images/avatars';
            user.username = req.body.username;
            user.email = req.body.email;
            // default role_id for user
            user.role_id = 2;

            const responseMessage = {
                userCreated: {
                    message: 'User has successfully registered'
                },
                userExists: {
                    message: 'User already exists'
                },
                databaseError: {
                    message: 'Can\'t register new user'
                }
            };

            user.username = req.body.username;
            user.email = req.body.email;
            user.avatar = generateImage(pathToAvatars);
            bcrypt.hash(req.body.password, saltRounds, hashingCb);

            function hashingCb(err, hash) {
                user.password = hash;

                db.User
                    .findOrCreate({
                        where: {email: user.email},
                        defaults: {
                            username: user.username, password: user.password,
                            avatar: user.avatar, role_id: user.role_id
                        }
                    })
                    .spread((createdUser, isCreated) => {
                        if (isCreated) {
                            return res.status(200).send(responseMessage.userCreated);
                        }
                        return res.status(409).send(responseMessage.userExists);
                    })
                    .catch((error) => res.status(400).send(`${responseMessage.databaseError}. ${error.message}`));
            }
        });

    app.post('/login', passport.authenticate(
        'local', {
            session: false
        }),
        serialize,
        generateToken,
        respond);

    function serialize(req, res, next) {
        // we store the updated information in req.user again so that we have no access to req.user.password
        req.user = {
            username: req.user.username,
            role_id: req.user.role_id,
            email: req.user.email,
            id: req.user.id
        };
        next();
    }

    function generateToken(req, res, next) {
        req.token = jwt.sign(req.user, config.server_secret, {
            expiresIn: 60 * 60 * 1000
        });
        next();
    }

    function respond(req, res) {
        res.status(200).json({
            user: req.user,
            token: req.token
        });
    }

    /**
     * Generates svg image
     * @param path
     * @returns string Url to created image
     */
    function generateImage(path) {
        const size = 150;
        const hex = 'abcdef0123456789';
        const hash = generateHash(hex, 16);
        const url = `${path}/${hash}.svg`;
        const svg = jdenticon.toSvg(hash, size);

        fs.writeFileSync(url, svg);
        return `${config.server_root}/${url}`;
    }

    /**
     * Generates random hash of exact length out of possible characters
     * @param possibleChars
     * @param hashLength
     * @returns {string}
     */
    function generateHash(possibleChars, hashLength) {
        let hash = '';

        for (let i = 0; i < hashLength; i++) {
            hash += possibleChars.charAt(Math.floor(Math.random() * possibleChars.length));
        }
        return hash;
    }

    app.get('/logout', (req, res) => {
        req.logout();
        res.send();
    });

    passport.use(new GoogleStrategy({
        clientID: googleAuthConfig.web.client_id,
        clientSecret: googleAuthConfig.web.client_secret,
        callbackURL: googleAuthConfig.web.redirect_uris[0]
    },
        (accessToken, refreshToken, profile, done) => {
            db.User
                .findOrCreate({
                    where: {
                        email: profile.emails[0].value
                    },
                    defaults: {
                        username: profile.displayName,
                        role_id: 2,
                        email: profile.emails[0].value,
                        googleId: profile.id,
                        avatar: profile.photos[0].value
                    }
                })
                .spread((user) => done(null, user));
        }
    ));

    app.get('/auth/google',
        passport.authenticate('google', {
            scope: ['openid email profile']
        }));

    function serializeGoogleUser(req, res, next) {
        const user = {
            id: req.user.dataValues.id,
            username: req.user.username,
            role_id: req.user.role_id,
            email: req.user.email
        };
        req.user = user;
        next();
    }

    function respondUser(req, res) {
        res.cookie('user', JSON.stringify({
            user: req.user,
            token: req.token
        }));
        res.redirect(`${config.client_root}/#/index`);
    }

    app.get('/auth/google/callback',
        passport.authenticate('google', {
            session: false
        }),
        serializeGoogleUser,
        generateToken,
        respondUser);

    passport.use(new GitHubStrategy({
        clientID: gitHubAuthConfig.client_id,
        clientSecret: gitHubAuthConfig.client_secret,
        callbackURL: gitHubAuthConfig.redirect_uri,
        scope: ['user:email']
    },
        (accessToken, refreshToken, profile, done) => {
            db.User
                .findOrCreate({
                    where: {
                        email: profile.emails[0].value
                    },
                    defaults: {
                        username: profile.username,
                        role_id: 2,
                        email: profile.emails[0].value,
                        githubId: profile.id,
                        avatar: profile._json.avatar_url
                    }
                })
                .spread((user) => done(null, user));
        }
    ));

    function serializeUser(req, res, next) {
        const user = {
            id: req.user.id,
            username: req.user.username,
            role_id: req.user.role_id,
            email: req.user.email
        };
        req.user = user;
        next();
    }

    app.get('/auth/github',
        passport.authenticate('github', {scope: ['user:email']}));

    app.get('/auth/github/callback',
        passport.authenticate('github', {
            session: false
        }),
        serializeUser,
        generateToken,
        respondUser);

    passport.use(new FacebookStrategy({
        clientID: facebookAuthConfig.client_id,
        clientSecret: facebookAuthConfig.client_secret,
        callbackURL: facebookAuthConfig.redirect_uri,
        profileFields: ['id', 'displayName', 'photos', 'email'],
        enableProof: true,
        session: false
    },
        (accessToken, refreshToken, profile, done) => {
            db.User
                .findOrCreate({
                    where: {
                        email: profile.emails[0].value
                    },
                    defaults: {
                        username: profile.displayName,
                        role_id: 2,
                        email: profile.emails[0].value,
                        avatar: profile.photos[0].value
                    }
                })
                .spread((user) => done(null, user));
        }
    ));

    app.get('/auth/facebook',
        passport.authenticate('facebook', {session: false, scope: ['public_profile', 'email']}));

    app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {
            session: false
        }),
        serializeUser,
        generateToken,
        respondUser);
};
