'use strict';
const DEFAULT_PORT = 3000;
const path = require('path');
const rootPath = path.normalize(`${__dirname}/..`);
const env = process.env.NODE_ENV || 'development';

const config = {
    development: {
        server_secret: 'server secret',
        root: rootPath,
        app: {
            name: 'server'
        },
        port: process.env.PORT || DEFAULT_PORT,
        db: 'sqlite://localhost/server-development',
        storage: `${rootPath}/data/server-development`,
        server_root: 'http://localhost:3000',
        client_root: 'http://localhost:8088',
        dest: 'public/images/avatars/'
    },

    test: {
        server_secret: 'server secret',
        root: rootPath,
        app: {
            name: 'server'
        },
        port: process.env.PORT || DEFAULT_PORT,
        db: 'sqlite://localhost/server-test',
        storage: `${rootPath}/data/server-test`
    },

    production: {
        server_secret: 'server secret',
        root: rootPath,
        app: {
            name: 'server'
        },
        port: process.env.PORT || DEFAULT_PORT,
        db: 'sqlite://localhost/server-production',
        storage: `${rootPath}data/server-production`
    }
};

module.exports = config[env];
