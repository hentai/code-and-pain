/* eslint no-console: "off" */

'use strict';
const express = require('express');
const config = require('./config/config');
const db = require('./app/models');
const app = express();
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const logger = require('morgan');
const compress = require('compression');
const router = require('./app/controllers');

const env = process.env.NODE_ENV || 'development';
app.locals.ENV = env;
app.locals.ENV_DEVELOPMENT = env === 'development';
app.use(logger('dev'));
app.use(compress());

const cors = require('cors');
const corsOptions = {
    origin: '*',
    methods: ['GET', 'PUT', 'POST', 'OPTIONS', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Authorization', 'Content-Length', 'X-Requested-With']
};
app.use(cors(corsOptions));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(bodyParserJsonErrorHandler);

require('./config/passport')(app);

app.use('/api', router);
app.use('/docs', express.static('../server/swagger'));
app.use('/public', express.static('../server/public'));

function bodyParserJsonErrorHandler(err, req, res, next) {
    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        console.error('Bad JSON');
        res.status(422).send({message: 'Can\'t parse request data'});
    }
    next();
}

function startServer() {
    app.listen(config.port, () => {
        console.log(`Express server listening on port ${config.port}`);
    });
}

db.sequelize
    .sync()
    .then(startServer)
    .catch((e) => {
        throw new Error(e);
    });
