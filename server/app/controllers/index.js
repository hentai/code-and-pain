'use strict';
const express = require('express');
const router = express.Router();
const models = require('../models');

router.use('/articles', require('./article'));
router.use('/tags', require('./tag'));
router.use('/compile', require('./compile'));
router.use('/users', require('./user'));
router.use('/snippets', require('./snippet'));
router.use('/avatar', require('./avatar'));
router.use('/snapshots', require('./snapshot'));

router.get('/recent', (request, response) => {
    models.Article.findAll({
        limit: 3,
        order: '"createdAt" DESC'
    })
        .then(articles => response.status(200).send(articles));
});

module.exports = router;
