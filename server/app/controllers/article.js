'use strict';

const models = require('../models');
const express = require('express');
const router = express.Router();
const jsonParser = require('body-parser').json();

const config = require('../../config/config');
const authenticate = require('express-jwt')({secret: config.server_secret});


const aliasError = {
    message: 'Alias already exists'
};
const deletingError = {
    message: 'Already deleted'
};


function createArticle(request) {
    return models.Article.create({
        title: request.body.title,
        alias: request.body.alias,
        text: request.body.text,
        user_id: request.user.id
    });
}

function getArticleId(articleAlias) {
    return models.Article.find({
        where: {
            alias: articleAlias
        }
    }).then((article) => article.id);
}

function addArticleTag(tagId, articleId) {
    return models.ArticleTag.create({
        article_id: articleId,
        tag_id: tagId
    });
}

function updateArticleByAlias(request, articleAlias) {
    return models.Article.update({
        title: request.body.title,
        alias: request.body.alias,
        text: request.body.text
    }, {where: {alias: articleAlias}})
        .then(
            () => models.Article.findOne({where: {alias: articleAlias}})
        );
}

function checkAlias(alias, aliasOld) {
    return models.Article.count({
        where: {alias}
    })
        .then((count) => {
            if (count !== 0 && alias !== aliasOld) {
                throw (aliasError);
            }
        });
}

router.head('/:alias_to_check', (request, response) => {
    checkAlias(request.params.alias_to_check)
        .then(() =>
            response.send(404)
        )
        .catch(() =>
            response.send(200)
        );
});

router.get('/', (request, response) => {
    if (request.query.tag) {
        models.Tag.findOne({
            where: {
                name: request.query.tag
            },
            include: [
                {
                    attributes: ['title', 'alias', 'updatedAt'],
                    all: true, include: [
                        {
                            model: models.User,
                            attributes: ['id', 'username', 'avatar']
                        }
                    ]
                }
            ]
        })
            .then(tag => response.status(200).send(tag.Articles));
    } else {
        models.Article.findAll({
            attributes: ['title', 'alias', 'updatedAt'],
            include: [
                {
                    model: models.User,
                    attributes: ['id', 'username', 'avatar']
                }
            ]
        })
            .then(articles => response.status(200).send(articles));
    }
});

router.post('/', authenticate, jsonParser, (request, response) => {
    createArticle(request)
        .then((data) => {
            response.send(data);
            getArticleId(request.body.alias).then((id) => {
                const tags = request.body.tags;
                for (let i = 0; i < tags.length; i++) {
                    addArticleTag(tags[i].id, id);
                }
            });
        })
        .catch(error => {
            response.status(400).send({
                message: error
            });
        });
});

router.put('/:alias', jsonParser, (request, response) => {
    checkAlias(request.body.alias, request.body.aliasOld).then(() =>
        updateArticleByAlias(request, request.params.alias)
            .then(data => {
                response.status(200).send(data);
            })
            .catch(error => response.status(400).send({
                message: error
            }))
    );
});


router.get('/user/:id', (request, response) => {
    models.Article.findAll({
        where: {
            user_id: request.params.id
        }
    }).then(
        (article) => {
            if (article) {
                return response.status(200).send(article);
            }
            return response.status(400).send({
                message: 'No articles found with the following user_id.'
            });
        });
});

router.delete('/user/:id', (request, response) => {
    models.Article.findAll({
        where: {
            user_id: request.params.id
        }
    })
        .then(article => {
            if (article) {
                models.Article.destroy({
                    where: {
                        user_id: request.params.id,
                        deletedAt: null
                    }
                });
            } else {
                throw deletingError;
            }
        }
        )
        .then(() => response.sendStatus(200))
        .catch(error => response.status(400).send(error));
});

router.get('/:alias', (request, response) => {
    models.Article.find({
        where: {
            alias: request.params.alias
        },
        include: [
            {
                model: models.User,
                attributes: ['id', 'username', 'avatar']
            },
            {
                all: true
            }
        ]
    }).then(
        (article) => {
            if (article) {
                return response.status(200).send(article);
            }
            return response.status(400).send({
                message: 'No articles found with the following alias.'
            });
        });
});

router.delete('/:alias', (request, response) => {
    models.Article.find({
        where: {
            alias: request.params.alias
        }
    })
        .then((article) => {
            if (article) {
                models.Article.destroy({
                    where: {
                        alias: article.alias,
                        deletedAt: null
                    }
                });
            } else {
                throw deletingError;
            }
        }
        )
        .then(() => response.sendStatus(200))
        .catch(error => response.status(400).send(error));
});

module.exports = router;
