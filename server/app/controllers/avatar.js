'use strict';
const router = require('express').Router();
const multer = require('multer');
const models = require('../models');
const config = require('../../config/config');
const crypto = require('crypto');
const path = require('path');

const storage = multer.diskStorage({
    destination: config.dest,
    filename(req, file, cb) {
        crypto.pseudoRandomBytes(16, (err, raw) => {
            if (err) return cb(err);
            return cb(null, raw.toString('hex') + path.extname(file.originalname));
        });
    }
});

const upload = multer({storage});

router.post('/:user_id', upload.single('avatar'), (request, response) => {
    const userId = request.params.user_id;
    const PATH = `${config.server_root}/${request.file.path}`;

    updateAvatar(PATH, userId);
    response.send({'data': PATH});
});

function updateAvatar(PATH, id) {
    models.User.update({
        avatar: PATH
    }, {where: {id}});
}

module.exports = router;
