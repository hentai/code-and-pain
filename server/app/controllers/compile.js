'use strict';
const router = require('express').Router();
const jsonParser = require('body-parser').json();
const TypeScriptSimple = require('typescript-simple').TypeScriptSimple;
const defaultTSOptions = {allowJS: true};

/**
 * Compiler with possible options param (for future POSTreq with options).
 *
 * @param {Object} code - Object with code to compile
 * @param {Object} [options=defaultTSOptions] - Compiler options
 * @returns {Object} - Object with compiled code or error log
 */
function compileCode(code, options = defaultTSOptions) {
    const tss = new TypeScriptSimple(options);

    try {
        return {
            code: tss.compile(code)
        };
    } catch (error) {
        return {
            error: error.message
        };
    }
}

router.post('/', jsonParser, (request, response) => {
    const compileResult = compileCode(request.body.code);
    let status = 200;
    if ('error' in compileResult) {
        status = 418;
        compileResult.message = 'Transpiling error';
        response.status(status).send(compileResult);
    } else {
        response.status(status).send(compileResult);
    }
})
;

module.exports = router;
