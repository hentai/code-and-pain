'use strict';

const models = require('../models');
const express = require('express');
const router = express.Router();

router.get('/', (request, response) => {
    models.Tag.findAll().then((tags) => {
        response.send({tags});
    });
});

router.post('/', (request, response) => {
    const names = request.body.map(tag => tag.name);

    models.Tag.bulkCreate(request.body).then(models.Tag.findAll({
        where: {
            name: {
                in: names
            }
        }
    }).then(data => response.send(data)));
});

module.exports = router;
