'use strict';

const models = require('../models');
const express = require('express');
const router = express.Router();
const jsonParser = require('body-parser').json();

/**
 *
 * @param data - data about user that need to be changed
 * @param id - user's id
 * @returns {*} - updated User
 */
function updateUser(data, id) {
    return models.User.update(data, {where: {id}}).then(
        () => models.User.findById(id)
    );
}

router.put('/:user_id', jsonParser, (request, response) => {
    const userData = {
        description: request.body.description,
        role_id: request.body.role_id,
        username: request.body.username,
        email: request.body.email,
        isBanned: request.body.isBanned
    };
    const userId = request.params.user_id;

    updateUser(userData, userId)
        .then(data => response.send(data))
        .catch((error) => response.status(400).send({'message': error.message}));
});

router.get('/', (request, response) => {
    models.User.findAll().then((users) => {
        response.send(users);
    });
});

router.get('/:user_id', (request, response) => {
    models.User.find({
        attributes: {exclude: ['password']},
        where: {
            id: request.params.user_id
        }
    })
        .then(
            (user) => {
                if (user) {
                    return response.status(200).send(user);
                }
                return response.sendStatus(400);
            });
});

module.exports = router;
