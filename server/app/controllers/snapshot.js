'use strict';

const models = require('../models');
const express = require('express');
const router = express.Router();
const jsonParser = require('body-parser').json();


function createSnapshot(request) {
    return models.Snapshot.create({
        name: request.body.title,
        code: request.body.code,
        user_id: request.body.user_id
    });
}

function updateSnapshotById(request, SnapshotId) {
    return models.Snapshot.update({
        name: '',
        code: request.body.code,
        user_id: request.body.user_id
    }, {where: {id: SnapshotId}})
        .then(
            () => models.Snapshot.findOne({where: {id: SnapshotId}})
        );
}

router.get('/', (request, response) => {
    if (request.query.user_id) {
        models.Snapshot.findAll({
            where: {
                user_id: request.query.user_id
            }
        })
            .then(Snapshot => response.status(200).send(Snapshot));
    } else {
        models.Snapshot.findAll().then((Snapshot) => {
            response.status(200).send(Snapshot);
        });
    }
});

router.get('/:id', (request, response) => {
    models.Snapshot.findOne({
        where: {
            id: request.params.id
        }
    })
        .then(Snapshot => response.status(200).send(Snapshot));
});

router.post('/', jsonParser, (request, response) => {
    createSnapshot(request)
        .then(data => response.send(data))
        .catch(error => {
            response.status(400).send({
                message: error
            });
        });
});

router.put('/:id', jsonParser, (request, response) => {
    updateSnapshotById(request, request.params.id)
        .then(data => {
            response.status(200).send(data);
        })
        .catch(error => response.status(400).send({
            message: error
        }));
});


module.exports = router;
