'use strict';

const models = require('../models');
const express = require('express');
const router = express.Router();
const jsonParser = require('body-parser').json();


function createSnippet(request) {
    return models.Snippet.create({
        name: request.body.title,
        code: request.body.code,
        user_id: request.body.user_id
    });
}

function updateSnippetById(request, snippetId) {
    return models.Snippet.update({
        name: '',
        code: request.body.code,
        user_id: request.body.user_id
    }, {where: {id: snippetId}})
        .then(
            () => models.Snippet.findOne({where: {id: snippetId}})
        );
}

router.get('/', (request, response) => {
    if (request.query.user_id) {
        models.Snippet.findAll({
            where: {
                user_id: request.query.user_id
            }
        })
                .then(snippet => response.status(200).send(snippet));
    } else {
        models.Snippet.findAll().then((snippet) => {
            response.status(200).send(snippet);
        });
    }
});

router.get('/:id', (request, response) => {
    models.Snippet.findOne({
        where: {
            id: request.params.id
        }
    })
        .then(snippet => response.status(200).send(snippet));
});

router.post('/', jsonParser, (request, response) => {
    createSnippet(request)
        .then(data => response.send(data))
        .catch(error => {
            response.status(400).send({
                message: error
            });
        });
});

router.put('/:id', jsonParser, (request, response) => {
    updateSnippetById(request, request.params.id)
            .then(data => {
                response.status(200).send(data);
            })
            .catch(error => response.status(400).send({
                message: error
            }));
});


module.exports = router;
