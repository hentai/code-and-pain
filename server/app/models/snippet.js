'use strict';

module.exports = function (sequelize, DataTypes) {
    const Snippet = sequelize.define('Snippet',
        {
            name: {
                type: DataTypes.STRING(64),
                allowNull: false
            },
            code: {
                type: DataTypes.TEXT,
                allowNull: false
            }
        }, {
            classMethods: {
                timestamps: true,
                createdAt: 'createdAt',
                updatedAt: 'updatedAt',
                associate: (models) => {
                    Snippet.belongsTo(models.User, {foreignKey: 'user_id', foreignKeyConstraint: true});
                }
            }
        }
    );

    return Snippet;
};
