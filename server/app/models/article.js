'use strict';

module.exports = function (sequelize, DataTypes) {
    const Article = sequelize.define('Article',
        {
            title: {
                type: DataTypes.STRING(64),
                allowNull: false,
                unique: true
            },
            alias: {
                type: DataTypes.STRING(64),
                allowNull: false,
                unique: true
            },
            text: {
                type: DataTypes.TEXT,
                allowNull: false
            }
        }, {
            timestamps: true,
            createdAt: 'createdAt',
            deletedAt: 'deletedAt',
            updatedAt: 'updatedAt',
            paranoid: true,
            underscored: true,
            classMethods: {
                associate: (models) => {
                    Article.belongsTo(models.User, {foreignKey: 'user_id', foreignKeyConstraint: true});
                    Article.belongsToMany(models.Tag, {
                        through: {
                            model: models.ArticleTag
                        },
                        foreignKey: 'article_id',
                        as: 'Tags'
                    });
                }
            }
        }
    );

    return Article;
};
