'use strict';

module.exports = function (sequelize, DataTypes) {
    const Tag = sequelize.define('Tag', {
        name: DataTypes.STRING
    }, {
        classMethods: {
            associate: (models) => {
                Tag.belongsToMany(models.Article, {
                    through: {
                        model: models.ArticleTag
                    },
                    foreignKey: 'tag_id',
                    as: 'Articles'
                });
            }
        }
    });
    return Tag;
};
