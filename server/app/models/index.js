'use strict';

const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const config = require('../../config/config');
const examplePost = require('../seed/post-example');
const admin = require('../seed/user-admin');
const userTest = require('../seed/user-test');
const userBanned = require('../seed/user-banned');
const tagsDefault = require('../seed/tags-default');
const userRoles = require('../seed/user-roles');
const articleWithTags = require('../seed/article-with-some-tags');
const snippets = require('../seed/snippet');
const snapshots = require('../seed/snapshot');

const sequelize = new Sequelize(config.db, {
    storage: config.storage,
    omitNull: true
});

const db = {};
db.sequelize = sequelize;
db.Sequelize = Sequelize;

fs
    .readdirSync(__dirname)
    .filter(file => (file.indexOf('.') !== 0) && (file !== 'index.js'))
    .forEach((file) => {
        const model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach((modelName) => {
    if ('associate' in db[modelName]) {
        db[modelName].associate(db);
    }
});

/**
 * Creates tables User, Tag, Article, Roles, Snippet
 */
const userPromise = db.User.sync({force: true});
const tagPromise = db.Tag.sync({force: true});
const articlePromise = db.Article.sync({force: true});
const rolePromise = db.Roles.sync({force: true});
const tagArticlePromise = db.ArticleTag.sync({force: true});
const snippetPromise = db.Snippet.sync({force: true});
const snapshotPromise = db.Snapshot.sync({force: true});
/**
 * Creates User, Article, Tag, Role instances
 */
Promise.all([userPromise, tagPromise, articlePromise, rolePromise, tagArticlePromise, snippetPromise, snapshotPromise])
    .then(addRoles)
    .then(() => db.User.create(admin))
    .then(() => db.User.create(userTest))
    .then(() => db.User.create(userBanned))
    .then(addArticles)
    .then(addTagsToArticle)
    .then(addTags)
    .then(addSnippet)
    .then(addSnapshot);


/**
 *
 * Adds default tags to example article
 * @param article
 */
function addTagsToArticle(article) {
    tagsDefault.forEach(tag => {
        db.Tag.create(tag)
            .then(createdTag => {
                article.forEach(example => example.addTag(createdTag));
            });
    });
    return db.Tag.findAll();
}

function addTags(tags) {
    db.Article.create(articleWithTags)
        .then(article => {
            tags.forEach(tag => {
                if (tag.id % Math.floor(Math.random() * 5 + 1) === 0) {
                    article.addTag(tag);
                }
            });
        });
}
/**
 * Adds default tags to example article
 */
function addRoles() {
    userRoles.forEach(role => {
        db.Roles.create(role);
    });
}

function addArticles() {
    examplePost.forEach(article => {
        db.Article.create(article);
    });
    return db.Article.findAll();
}

function addSnippet() {
    snippets.forEach(snippet => {
        db.Snippet.create(snippet);
    });
}

function addSnapshot() {
    snapshots.forEach(snapshot => {
        db.Snapshot.create(snapshot);
    });
}
module.exports = db;
