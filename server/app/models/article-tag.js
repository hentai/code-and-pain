'use strict';

module.exports = function (sequelize, DataTypes) {
    const ArticleTag = sequelize.define('ArticleTag', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        }
    });
    return ArticleTag;
};
