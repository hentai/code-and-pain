'use strict';

module.exports = function (sequelize, DataTypes) {
    const Roles = sequelize.define('Roles', {
        role: {
            type: DataTypes.STRING(32),
            allowNull: false
        }
    }, {
        classMethods: {
            associate: (models) => {
                Roles.hasMany(models.User, {foreignKey: 'role_id', as: 'Users'});
            }
        }
    });
    return Roles;
};
