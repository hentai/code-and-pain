'use strict';

module.exports = function (sequelize, DataTypes) {
    const User = sequelize.define('User', {
        username: {
            type: DataTypes.STRING(32),
            allowNull: false,
            unique: true,
            validate: {
                notEmpty: true
            }
        },
        googleId: {
            type: DataTypes.INTEGER,
            allowNull: true,
            unique: true
        },
        githubId: {
            type: DataTypes.INTEGER,
            allowNull: true,
            unique: true
        },
        email: {
            type: DataTypes.STRING(32),
            allowNull: false,
            unique: true,
            validate: {
                notEmpty: true,
                isEmail: true
            }
        },
        password: {
            type: DataTypes.STRING(32),
            allowNull: true
        },
        loggedIn: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        avatar: {
            type: DataTypes.STRING
        },
        description: {
            type: DataTypes.TEXT
        },
        isBanned: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        isDeleted: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    }, {
        freezeTableName: true,
        timestamps: true,
        createdAt: 'createdAt',
        updatedAt: 'updatedAt',
        underscored: true,
        classMethods: {
            associate: (models) => {
                User.hasMany(models.Article, {foreignKey: 'user_id', as: 'Articles'});
                User.hasMany(models.Snippet, {foreignKey: 'user_id', as: 'Snippets'});
                User.belongsTo(models.Roles, {foreignKey: 'role_id', foreignKeyConstraint: true});
            }
        }
    });

    return User;
};
