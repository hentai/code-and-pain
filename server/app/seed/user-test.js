'use strict';

module.exports = {
    username: 'Test',
    email: 'test@gmail.com',
    password: '$2a$10$TR/4PkqcC6D1F/WFVJS1aO5ZYhsjXPjOOv0RaDfOjqFPUQnQjoXaK',
    avatar: 'https://www.looktracker.com/wp-content/uploads/2014/06/Choose-Eye-Tracking-User-Test-Mindset.png',
    description: 'Test user.',
    role_id: 2
};
