'use strict';

module.exports = [{
    name: 'fori',
    code: `for(let i=0; i<; i++ ){
    
}`,
    user_id: 1
}, {
    name: 'us',
    code: '\'use strict\'',
    user_id: 2
}, {
    name: 'cl',
    code: 'console.log()',
    user_id: 2
}, {
    name: 'cd',
    code: 'console.dir()',
    user_id: 2
}, {
    name: 'fn',
    code: `function(){
        
}`,
    user_id: 2
}, {
    name: 'jp',
    code: 'JSON.parse()',
    user_id: 2
}, {
    name: 'js',
    code: 'JSON.stringify()',
    user_id: 2
}, {
    name: 'st',
    code: 'setTimeout(function(){}, delay)',
    user_id: 2
}, {
    name: 'si',
    code: 'setInterval(function(){}, delay)',
    user_id: 2
}, {
    name: 'fin',
    code: `for(key in obj){
    
}`,
    user_id: 2
}, {
    name: 'ts',
    code: `interface Drivable {   

    start(): void;
    
    drive(distance: number): boolean;
    getPosition(): number;
}

class Car implements Drivable {
    private _isRunning: boolean;
    private _distanceFromStart: number;

    constructor() {
        this._isRunning = false;
        this._distanceFromStart = 0;
    }

    public start() {
        this._isRunning = true;
    }

    public drive(distance: number): boolean {
        if (this._isRunning) {
            this._distanceFromStart += distance;
            return true;
        }
        return false;
    }

    public getPosition(): number {
        return this._distanceFromStart;
    }
}

class LittleCar extends Car {
  d(){}
}

let lc = new LittleCar();
lc.drive(2);`,
    user_id: 2
}];
