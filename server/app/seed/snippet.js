'use strict';

module.exports = [{
    name: 'Loop',
    code: 'for(int i=0; i<10; i++ ){}',
    user_id: 1
}, {
    name: 'Loop adv',
    code: 'for(int i=0; i<10000000; i++ ){}',
    user_id: 2
}];
