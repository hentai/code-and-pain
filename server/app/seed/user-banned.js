'use strict';

module.exports = {
    username: 'Banned',
    email: 'banned@gmail.com',
    password: '$2a$10$TR/4PkqcC6D1F/WFVJS1aO5ZYhsjXPjOOv0RaDfOjqFPUQnQjoXaK',
    avatar: 'http://rito-ban.com/wp-content/uploads/user_forbidden_man_male_profile_account_person-5121.png',
    description: 'Banned user.',
    isBanned: true,
    role_id: 2
};
