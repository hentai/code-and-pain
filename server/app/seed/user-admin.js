'use strict';

const SERVER_ROOT = 'http://localhost:3000';

module.exports = {
    username: 'Admin',
    email: 'admin@gmail.com',
    password: '$2a$10$TR/4PkqcC6D1F/WFVJS1aO5ZYhsjXPjOOv0RaDfOjqFPUQnQjoXaK',
    avatar: `${SERVER_ROOT}/public/images/avatars/admin.svg`,
    description: 'Web UI developer. Lives in Kiev.',
    role_id: 1
};
