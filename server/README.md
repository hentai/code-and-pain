# PainAndCode-node-generator

Generator for CodeAndPain project. Using Express-generator with some updatings.

## Installation
To obtain working backend:

1. Clone https://github.com/petecoop/generator-express and follow instructions.
2. Chose MVC version, Jade engine, None pre-processors, Gulp and SQLite database.
3. Open created project.
4. Delete "app/view", "public" directories and "bower.json".
5. Open main project.
6. Create new directory "server" and add all files from the generator.

## Commands
Open folder 'server'

1. Installation: npm install
2. Run: npm start
