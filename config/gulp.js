'use strict';

module.exports = {
    paths: {
        build: {
            output: 'build',
            outputFonts: 'build/components/application/fonts'
        },
        sources: ['client/src/**/*.js', 'server/app/**/*.js', 'server/config/**/*.js'],
        configs: ['client/config/**/!(system).js', 'gulp-tasks/**/*.js', 'gulpfile.js'],
        stylesheets: ['client/src/**/*.scss'],
        scripts: [
            'client/src/**/*.js',
            'server/app/**/*.js',
            'server/config/**/*.js',
            'server/*.js',
            'gulpfile.js'
        ],
        html: [
            'client/src/**/*.html',
            'client/index.html'
        ],
        static: [
            '.client/src/**/*.json',
            '.client/src/**/*.svg',
            '.client/src/**/*.woff',
            '.client/src/**/*.woff2',
            '.client/src/**/*.ttf',
            '.client/src/**/*.png',
            '.client/src/**/*.gif',
            '.client/src/**/*.ico',
            '.client/src/**/*.jpg',
            '.client/src/**/*.eot'
        ]
    },
    serverPort: 8088,
    serverPortTest: 8089,
    livereload: false,
    notifications: true
};
