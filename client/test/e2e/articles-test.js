const chai = require('chai');
const promised = require('chai-as-promised');
chai.use(promised);
const expect = chai.expect;


describe('Articles State', () => {
    beforeEach(() => {
        browser.get('http://localhost:8088/#/blog/articles');
        browser.ignoreSynchronization = true;
        browser.waitForAngular();
        browser.sleep(1000);
    });

    it('should verify page has title ', () => {
        const blogTitle = element(by.css('h1'));
        expect(blogTitle.isPresent()).to.eventually.be.true;
    });
});
