'use strict';

const plumber = require('gulp-plumber');

module.exports = (gulp, config) => {
    gulp.task('include-font-awesome', () => gulp
        .src('node_modules/font-awesome/fonts/**/*.{otf,ttf,woff,woff2,eof,eot,svg}')
        .pipe(plumber())
        .pipe(gulp.dest(config.paths.build.outputFonts))
    );
};
