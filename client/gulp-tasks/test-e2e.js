'use strict';

const connect = require('gulp-connect');
const protractor = require('gulp-protractor');
const selenium = require('selenium-standalone');


module.exports = (gulp, config) => {
    gulp.task('selenium', (done) => {
        selenium.install({logger: console.log}, () => {
            selenium.start(done);
        });
    });
    gulp.task('test-e2e', ['build', 'webdriver-update'], (done) => {
        connect.server({
            port: config.serverPortTest,
            root: ['.']
        });

        gulp
            .src(`${config.paths.e2e}/*-test.js`)
            .pipe(protractor.protractor({
                configFile: `${__dirname}/../config/protractor.js`
            }))
            .on('error', (err) => {
                connect.serverClose();

                throw err;
            })
            .on('end', () => {
                connect.serverClose();

                done();
            });
    });
};
