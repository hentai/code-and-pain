import template from './users-state.html!text';

function usersRouteConfig($stateProvider) {
    $stateProvider
        .state('app.users', {
            url: 'users',
            forbidden: true,
            views: {
                application: {
                    controller: 'UsersStateController as usersState',
                    template
                }
            }
        });
}

export default [
    '$stateProvider',
    usersRouteConfig
];
