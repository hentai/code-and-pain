import './cap-users-list-component.css!';
import angular from 'angular';
import CapUsersListComponentController from './cap-users-list-component-controller';
import capUsersListComponent from './cap-users-list-component';

const dependencies = [

];

export default angular
    .module('cap-users-list-component', dependencies)
    .controller('CapUsersListComponentController', CapUsersListComponentController)
    .component('capUsersList', capUsersListComponent);
