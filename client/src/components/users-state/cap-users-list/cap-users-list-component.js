import template from './cap-users-list-component.html!text';

export default {
    bindings: {
        users: '<',
        onUpdateUser: '&'
    },
    bindToController: true,
    controller: 'CapUsersListComponentController',
    require: { },
    template
};
