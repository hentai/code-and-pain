class CapUsersListComponentController {
    constructor(USER, $state, $filter) {
        this.USER_ROLES = USER.ROLE;
        this.$state = $state;
        this.$filter = $filter;
        this.initialUsers = this.users;
        this.propertyToSortBy = 'username';
        this.propertyToFilterBy = 'role_id';
        this.defaultDescription = 'No description';
        this.$onChanges = this.onChangeUsersList;
        this.roles = this.getRoles(this.USER_ROLES);
        this.sortingProperties = this.getSortingProperties();
        this.selectedSortingProperty = this.propertyToSortBy;
    }

    /**
     * Sends user object to the parent component for update
     *
     * @param {Object} user - An user object
     */
    updateUser(user) {
        this.onUpdateUser({user});
    }

    banUser(user, banStatus) {
        user.isBanned = banStatus;
        this.onUpdateUser({user});
    }

    /**
     * Generates roles array based on constant.json file
     *
     * @param {Object} roles - Roles object
     * @return {Array} rolesArray - Generated roles array
     */
    getRoles(roles) {
        const rolesArray = [];

        for (const role in roles) {
            if ({}.hasOwnProperty.call(roles, role)) {
                rolesArray.push({
                    id: roles[role],
                    name: role.toLowerCase()
                });
            }
        }

        return rolesArray;
    }

    /**
     * Generates array contains properties to sort users by
     *
     * @return {Array} - Properties for users' sorting
     */
    getSortingProperties() {
        return [
            {
                property: 'username',
                name: 'username'
            },
            {
                property: 'createdAt',
                name: 'registered at'
            }
        ];
    }

    /**
     * Fires when users array was updated in the outside component
     *
     * @param {Object} changes - An object represents value changes
     */
    onChangeUsersList(changes) {
        if (!changes.users || !changes.users.currentValue) {
            return;
        }

        this.initialUsers = this.users = changes.users.currentValue;
        this.sortUsersBy(this.propertyToSortBy);
    }

    checkUserDescription(user) {
        return user.description || this.defaultDescription;
    }

    goToProfileState(id) {
        this.$state.go('app.profile', {id});
    }

    /**
     * Sorts users by property name in the each user object using Angular's $filter service
     *
     * @param {string} property - User's object property
     */
    sortUsersBy(property) {
        this.propertyToSortBy = property;
        let reverse = false;
        if (property === 'createdAt') {
            reverse = true;
        }
        this.users = this.$filter('orderBy')(this.users, property, reverse);
    }

    /**
     * Filters users by property name and compareWith value in the each user object. It uses custom usersFilter filter.
     * Call sorting after filtering.
     *
     * For reset filtered array pass property argument with boolean false value.
     *
     * @param {string} property - User's object property
     * @param {*} compareWith - Value to compare with
     */
    filterUsersBy(property, compareWith) {
        if (!property || !compareWith) {
            this.users = this.initialUsers;
        } else {
            this.users = this.initialUsers.filter((user) => {
                if (property in user && user[property] === compareWith) {
                    return true;
                }
                return false;
            });
        }

        this.sortUsersBy(this.propertyToSortBy);
    }
}

export default [
    'USER',
    '$state',
    '$filter',
    CapUsersListComponentController
];
