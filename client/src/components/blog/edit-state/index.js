import './edit-state.css!';
import angular from 'angular';
import 'angular-ui-router';
import EditStateController from './edit-state-controller';
import editRouteConfig from './edit-route';
import 'components/blog/cap-text-editor/index';
import capArticleFormDirective from './../cap-article-form/index';

const dependencies = [
    'ui.router',
    'cap-text-editor-component',
    capArticleFormDirective.name
];

export default angular
    .module('edit-state-component', dependencies)
    .controller('EditStateController', EditStateController)
    .config(editRouteConfig);
