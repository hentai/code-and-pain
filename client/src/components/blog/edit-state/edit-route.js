import template from './edit-state.html!text';

function editRouteConfig($stateProvider) {
    $stateProvider
        .state('app.blog.article.edit', {
            url: '/edit',
            views: {
                article: {
                    controller: 'EditStateController as editState',
                    template
                }
            }
        });
}

export default [
    '$stateProvider',
    editRouteConfig
];
