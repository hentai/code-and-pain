class EditStateController {

    constructor(ArticlesService, $state, authService) {
        this.ArticlesService = ArticlesService;
        this.$state = $state;
        this.authService = authService;
        this.article = ArticlesService.article;
        this.article.aliasOld = this.ArticlesService.article.alias;
    }

    setInputText(text) {
        this.text = text;
    }

    updateArticle() {
        this.ArticlesService.updateArticle(this.article)
            .then(response => this.$state.go('app.blog.article', {alias: response.config.data.alias}));
    }

    filterAlias(title) {
        const filtered = title.toLowerCase().replace(/[^a-z-]/g, '-');
        return filtered.replace(/-{2,}/g, '-');
    }
}

export default [
    'ArticlesService',
    '$state',
    'authService',
    EditStateController
];
