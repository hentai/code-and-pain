class CapArticleFormDirectiveController {
    constructor($http, API_URL, TagsService, ArticlesService, $state) {
        this.$http = $http;
        this.API_URL = API_URL;
        this.aliasIsTaken = false;
        this.TagsService = TagsService;
        this.ArticlesService = ArticlesService;
        this.tags = [];
        this.$state = $state;
        this.userTags = [];
        this.getTags();
        this.choiceToAddTags = ['Add tags by myself', 'Choice from existing'];
        this.userChoice = this.choiceToAddTags[0];
    }

    filterAlias() {
        if (!this.article && !this.article.title) {
            return;
        }
        this.article.alias = this.article.title.toLowerCase().replace(/[^a-z0-9-]/g, '-').replace(/-{2,}/g, '-');
    }

    checkIfArticleExists() {
        if (!this.article && !this.article.alias) {
            return;
        }

        this.$http.head(`${this.API_URL.BASE_API}${this.API_URL.ARTICLES}/${this.article.alias}`)
            .then(() => {
                this.aliasIsTaken = true;
            }).catch(() => {
                this.aliasIsTaken = false;
            });
    }

    getTags() {
        this.TagsService.getTags().then(response => {
            this.tags = response;
        });
    }

    getActiveTags(active) {
        this.article.activeTags = active;
    }

    addTags(tags) {
        this.TagsService.addTags(tags).then(createdTags => {
            if (this.article.activeTags) {
                this.article.activeTags = this.article.activeTags.concat(createdTags);
            } else {
                this.article.activeTags = createdTags;
            }
            this.createArticle();
        });
    }

    createArticle() {
        this.ArticlesService.createArticle(this.article)
            .then(response => this.$state.go('app.blog.article', {alias: response.config.data.alias}));
    }
}

export default [
    '$http',
    'API_URL',
    'TagsService',
    'ArticlesService',
    '$state',
    CapArticleFormDirectiveController
];
