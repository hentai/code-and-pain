import template from './cap-article-form-directive.html!text';
import angular from 'angular';

function capArticleFormDirective() {
    return {
        restrict: 'E',
        controller: 'CapArticleFormDirectiveController as $ctrl',
        scope: {},
        link: (scope, element) => {
            const addTagButton = element[0].querySelector('.add-tag-button');

            addTagButton.onclick = () => {
                const tagName = element[0].querySelector('.tags-input').value;
                const tag = {
                    name: tagName
                };
                scope.$ctrl.userTags.push(tag);
                const tagList = angular.element(document.getElementById('tag-list'));
                const tagItem = angular.element(`<span class="tag-list-item">${tagName}</span>`);

                if (tagItem) {
                    tagItem.on('click', (event) => {
                        event.target.remove();
                        for (let i = 0; i < scope.$ctrl.userTags.length; i++) {
                            if (scope.$ctrl.userTags[i].name === event.target.innerText) {
                                scope.$ctrl.userTags.splice(i, 1);
                            }
                        }
                    });
                }
                tagList.append(tagItem);
            };

            const createButton = document.getElementById('create-article-button');
            createButton.onclick = () => {
                scope.$ctrl.addTags(scope.$ctrl.userTags);
            };
        },
        bindToController: {
            article: '='
        },
        template
    };
}

export default[
    capArticleFormDirective
];
