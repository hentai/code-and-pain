import angular from 'angular';
import 'angular-mocks';
import component from './index';

describe('Cap Text Editor Component', () => {
    beforeEach(angular.mock.module(component.name));

    let $controller;

    beforeEach(angular.mock.inject((_$controller_) => {
        $controller = _$controller_;
    }));

    describe('Cap Text Editor Controller', () => {
        beforeEach(() => {
            $controller = $controller('CapTextEditorComponentController');
        });

        it('Check codemirror config keys', () => {
            expect($controller.getCodemirrorConfig()).to.have.property('mode');
            expect($controller.getCodemirrorConfig()).to.have.property('extraKeys');
            expect($controller.getCodemirrorConfig()).to.have.property('autofocus');
        });

        it('Check codemirror config values', () => {
            const config = {
                mode: {
                    name: 'markdown'
                },
                extraKeys: {
                    'F11': (cm) =>
                        cm.setOption('fullScreen', !cm.getOption('fullScreen')),
                    'Esc': (cm) => {
                        if (cm.getOption('fullScreen')) {
                            cm.setOption('fullScreen', false);
                        }
                    }
                },
                autofocus: true
            };
            expect($controller.getCodemirrorConfig().mode).to.deep.equal(config.mode);
            expect($controller.getCodemirrorConfig().autofocus).to.equal(config.autofocus);
            expect($controller.getCodemirrorConfig().extraKeys.length)
                .to.equal(config.extraKeys.length);
        });
    });
});
