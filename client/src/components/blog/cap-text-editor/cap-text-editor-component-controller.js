class CapTextEditorComponentController {
    constructor() {
        this.isPreviewMode = false;
    }

    /**
     * Gets CodeMirror configs to render a text editor.
     * @returns config config object
     */
    getCodemirrorConfig() {
        const config = {
            mode: {
                name: 'markdown'
            },
            theme: 'default',
            lineNumbers: true,
            lineWrapping: true,
            indentUnit: 4,
            extraKeys: {
                'F11': (cm) =>
                    cm.setOption('fullScreen', !cm.getOption('fullScreen')),
                'Esc': (cm) => {
                    if (cm.getOption('fullScreen')) {
                        cm.setOption('fullScreen', false);
                    }
                }
            },
            autofocus: true
        };
        return config;
    }
}

export default [
    CapTextEditorComponentController
];
