import CodeMirror from 'codemirror';
import markdown from 'markdown';

function createDirective() {
    return {
        link: (scope, element) => {
            const textArea = element[0].querySelector('#text-editor');
            const textEditor = CodeMirror.fromTextArea(textArea, scope.$ctrl.getCodemirrorConfig());
            if (scope.$ctrl.text) {
                textEditor.setValue(scope.$ctrl.text);
                const preview = markdown.parse(textEditor.getValue());
                const previewElement = element[0].querySelector('#preview');
                previewElement.innerHTML = preview;
            }

            /**
             * Adds listener for user inputText
             */
            function addChangeListener() {
                let inputText = '';
                let preview = '';
                const previewElement = element[0].querySelector('#preview');

                textEditor.on('change', () => {
                    inputText = textEditor.getValue();
                    preview = markdown.parse(inputText);
                    previewElement.innerHTML = preview;
                    scope.$ctrl.text = inputText;
                    scope.$apply();
                });
            }

            addChangeListener();
        }
    };
}

export default createDirective;
