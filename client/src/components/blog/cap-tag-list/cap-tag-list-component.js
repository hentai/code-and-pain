import template from './cap-tag-list-component.html!text';

export default {
    bindings: {
        tags: '<',
        displayAllTagName: '<',
        onTagSelected: '&',
        activeTag: '<',
        getActiveTags: '&'
    },
    bindToController: true,
    controller: 'CapTagListComponentController',
    require: { },
    template
};
