class CapTagListComponentController {
    constructor() {
        this.activeTag = {name: this.activeTag};
        this.activeTags = [];
        this.tagsToDisplay = [];
        this.$onChanges = (changes) => {
            if (this.displayAllTagName && changes.tags) {
                this.tagsToDisplay = [{name: this.displayAllTagName}, ...changes.tags.currentValue];
            } else if (changes.tags) {
                this.tagsToDisplay = changes.tags.currentValue;
            }
        };
    }

    onSelected(tag) {
        this.activeTag = tag;
        if (this.isSelectedTag(tag)) {
            const index = this.activeTags.indexOf(tag);
            this.activeTags.splice(index, 1);
        } else {
            this.activeTags.push(tag);
        }
        this.onTagSelected({tag: tag.name});
    }

    isSelectedTag(tag) {
        for (let i = 0; i < this.activeTags.length; i++) {
            if (tag === this.activeTags[i]) {
                return true;
            }
        }
        return false;
    }
}

export default [
    CapTagListComponentController
];
