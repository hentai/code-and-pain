import './cap-tag-list-component.css!';
import angular from 'angular';
import CapTagListComponentController from './cap-tag-list-component-controller';
import capTagListComponent from './cap-tag-list-component';

const dependencies = [
];

export default angular
    .module('cap-tag-list-component', dependencies)
    .controller('CapTagListComponentController', CapTagListComponentController)
    .component('capTagList', capTagListComponent);
