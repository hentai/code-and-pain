import './cap-article-component.css!';
import angular from 'angular';
import CapArticleComponentController from './cap-article-component-controller';
import capArticleComponent from './cap-article-component';
import 'angular-sanitize';

const dependencies = [
    'ngSanitize'
];

export default angular
    .module('cap-article-component', dependencies)
    .controller('CapArticleComponentController', CapArticleComponentController)
    .component('capArticle', capArticleComponent);
