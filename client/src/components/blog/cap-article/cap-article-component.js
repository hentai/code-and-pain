import template from './cap-article-component.html!text';

export default {
    bindings: {
        text: '<',
        recent: '<',
        article: '<'
    },
    bindToController: true,
    controller: 'CapArticleComponentController',
    require: {},
    template
};
