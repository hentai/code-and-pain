class CapArticleComponentController {
    constructor($sce, $state) {
        this.$state = $state;
        this.$sce = $sce;
    }
    showText(text) {
        return this.$sce.trustAsHtml(text);
    }
    showProfile(id) {
        this.$state.go('app.profile', id);
    }
    onSelectedTag(tag) {
        this.$state.go('app.blog.articles', {tag});
    }
}

export default [
    '$sce',
    '$state',
    CapArticleComponentController
];
