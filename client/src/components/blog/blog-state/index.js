import './blog-state.css!';
import angular from 'angular';
import 'angular-ui-router';
import BlogStateController from './blog-state-controller';
import ArticlesService from './../services/articles-service';
import TagsService from './../services/tags-service';
import blogRouteConfig from './blog-route';

const dependencies = [
    'ui.router'
];

export default angular
    .module('blog-state-component', dependencies)
    .controller('BlogStateController', BlogStateController)
    .service('ArticlesService', ArticlesService)
    .service('TagsService', TagsService)
    .config(blogRouteConfig);
