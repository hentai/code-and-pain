import markdown from 'markdown';
/**
 * Provides methods for retrieving articles from the server
 */
class ArticlesService {
    /**
     * @param {$q} $q - Angular $q service that helps you run functions asynchronously, and use their return values
     * (or exceptions) when they are done processing
     */
    constructor($q, $http, API_URL) {
        this.$http = $http;
        this.API_URL = API_URL;
        this.$q = $q;
        this.articles = [];
        this.markdownText = '';
        this.article = undefined;
        // Because controller initializes! Service just does things.
        // this.getArticles();
    }

    /**
     * Gets articles list from server and saves it into articles array property
     */
    getArticles() {
        return this.$http.get(`${this.API_URL.BASE_API}${this.API_URL.ARTICLES}`)
            .then(response => {
                this.articles = response.data;
                return this.articles;
            });
    }

    /**
     * Gets last five articles from server and saves it into articles array property
     */
    getRecentArticles() {
        return this.$http.get(`${this.API_URL.BASE_API}${this.API_URL.RECENT}`)
            .then(response => {
                this.recentarticles = response.data;
                return this.recentarticles;
            });
    }

    /**
     * Gets articles list from server by tag and saves it into articles array property
     */
    getArticlesByTag(tag) {
        return this.$http.get(`${this.API_URL.BASE_API}${this.API_URL.ARTICLES}?tag=${tag}`)
            .then(response => {
                this.articles = response.data;
                return this.articles;
            });
    }

    /**
     * Gets article by Alias from server and saves it into article property
     *
     * @param {string} alias - Article Alias
     */
    getArticle(alias) {
        return this.$http.get(`${this.API_URL.BASE_API}${this.API_URL.ARTICLES}/${alias}`)
            .then(response => {
                this.article = response.data;
                this.markdownText = markdown.parse(this.article.text);
                return response.data;
            });
    }

    updateArticle(data) {
        this.article.title = data.title;
        this.article.alias = data.alias;
        this.article.text = data.text;
        this.markdownText = markdown.parse(this.article.text);
        return this.$http.put(
            `${this.API_URL.BASE_API}${this.API_URL.ARTICLES}/${this.article.aliasOld}`, this.article);
    }

    /**
     * Creates new article in server DB by using data from article model
     *
     * @param {Object} data - Article model data
     * @param {string} data.alias - Article alias for URLs
     * @param {string} data.title - Article title
     * @param {string} data.text - Article text
     */
    createArticle(data) {
        const article = {
            title: data.title,
            alias: data.alias,
            text: data.text,
            tags: data.activeTags
        };
        return this.$http.post(this.API_URL.BASE_API + this.API_URL.ARTICLES, article);
    }

    /**
     * Deletes article by Alias from server
     *
     * @param alias
     */
    deleteArticle(alias) {
        return this.$http.delete(`${this.API_URL.BASE_API}${this.API_URL.ARTICLES}/${alias}`);
    }
}

export default [
    '$q',
    '$http',
    'API_URL',
    ArticlesService
];
