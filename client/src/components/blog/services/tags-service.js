/**
 * Provides methods for retrieving tags from the server
 */
class TagsService {
    /**
     * @param {$q} $q - Angular $q service that helps you run functions asynchronously, and use their return values
     * (or exceptions) when they are done processing
     */
    constructor($http, API_URL) {
        this.$http = $http;
        this.API_URL = API_URL;
        this.tags = [];
    }

    /**
     * Gets tags list from server and saves it into tags array property
     */
    getTags() {
        return this.$http.get(`${this.API_URL.BASE_API}${this.API_URL.TAGS}`)
            .then(response => {
                this.tags = response.data.tags;
                return this.tags;
            })
            .catch(e => e);
    }

    addTags(tags) {
        return this.$http.post(`${this.API_URL.BASE_API}${this.API_URL.TAGS}`, tags)
            .then(response => response.data);
    }
}

export default [
    '$http',
    'API_URL',
    TagsService
];
