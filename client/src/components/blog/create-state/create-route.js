import template from './create-state.html!text';

function createRouteConfig($stateProvider) {
    $stateProvider
        .state('app.blog.create', {
            url: '/create',
            views: {
                blog: {
                    controller: 'CreateStateController as createState',
                    template
                }
            }
        });
}

export default [
    '$stateProvider',
    createRouteConfig
];
