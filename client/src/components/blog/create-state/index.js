import './create-state.css!';
import angular from 'angular';
import 'angular-ui-router';
import CreateStateController from './create-state-controller';
import ArticlesService from './../services/articles-service';
import createRouteConfig from './create-route';
import capArticleFormDirective from './../cap-article-form/index';

const dependencies = [
    'ui.router',
    'cap-text-editor-component',
    capArticleFormDirective.name
];

export default angular
    .module('create-state-component', dependencies)
    .controller('CreateStateController', CreateStateController)
    .service('ArticlesService', ArticlesService)
    .config(createRouteConfig);
