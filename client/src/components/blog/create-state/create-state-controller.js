class CreateStateController {
    constructor(ArticlesService, $state, authService) {
        this.ArticlesService = ArticlesService;
        this.$state = $state;
        this.authService = authService;
        this.article = {};
    }
}

export default [
    'ArticlesService',
    '$state',
    'authService',
    CreateStateController
];
