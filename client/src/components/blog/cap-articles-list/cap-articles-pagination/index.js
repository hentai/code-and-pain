import './cap-articles-pagination-component.css!';
import angular from 'angular';
import CapArticlesPaginationComponentController from './cap-articles-pagination-component-controller';
import capArticlesPaginationComponent from './cap-articles-pagination-component';

const dependencies = [

];

export default angular
    .module('cap-articles-pagination-component', dependencies)
    .controller('CapArticlesPaginationComponentController', CapArticlesPaginationComponentController)
    .component('capArticlesPagination', capArticlesPaginationComponent);
