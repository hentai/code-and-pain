import template from './cap-articles-pagination-component.html!text';

export default {
    bindings: {
        loadPage: '&'
    },
    bindToController: true,
    controller: 'CapArticlesPaginationComponentController',
    require: { },
    template
};
