import './articles-state.css!';
import angular from 'angular';
import 'angular-ui-router';
import ArticlesStateController from './articles-state-controller';
import articlesRouteConfig from './articles-route';
import capTagListComponent from './../cap-tag-list/index';
import capArticlesListComponent from './../cap-articles-list/index';

const dependencies = [
    'ui.router',
    capArticlesListComponent.name,
    capTagListComponent.name
];

export default angular
    .module('articles-state-component', dependencies)
    .controller('ArticlesStateController', ArticlesStateController)
    .config(articlesRouteConfig);
