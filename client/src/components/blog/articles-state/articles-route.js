import template from './articles-state.html!text';

function articlesRouteConfig($stateProvider) {
    $stateProvider
        .state('app.blog.articles', {
            url: '/articles?tag',
            views: {
                blog: {
                    controller: 'ArticlesStateController as articlesState',
                    template
                }
            }
        });
}

export default [
    '$stateProvider',
    articlesRouteConfig
];
