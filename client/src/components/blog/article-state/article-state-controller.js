class ArticleStateController {
    /**
     * @param {$state} $state - AngularUI $state service is responsible for representing states and transitioning
     * between them
     * @param {$stateParams} $stateParams - AngularUI $stateParams service is an object that will have one key per url
     * parameter. The $stateParams is a perfect way to provide your controllers or other services with the individual
     * parts of the navigated url.
     * @param {ArticlesService} ArticlesService - Service provides retrieving articles from the server
     */
    constructor($state, $stateParams, ArticlesService, authService, USER) {
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.ArticlesService = ArticlesService;
        this.USER = USER;
        this.recent = [];
        this.authService = authService;
        this.article = {};
        this.ArticlesService.getRecentArticles()
            .then(() => {
                this.recent = this.ArticlesService.recentarticles;
            });
        this.ArticlesService.getArticle(this.$stateParams.alias)
            .then((article) => {
                if (!article) {
                    this.$state.go('app.blog.articles');
                }
                this.article = article;
            })
            .catch(() => {
                this.$state.go('app.blog.articles');
            });
    }

    getMarkdownText() {
        return this.ArticlesService.markdownText;
    }

    /**
     * Gets current article stored in the ArticlesService
     */
    getArticle() {
        return this.ArticlesService.article;
    }

    deleteArticle() {
        this.ArticlesService.deleteArticle(this.$stateParams.alias);
    }

    isAdmin() {
        const userRole = this.authService.getCurrentUser().role_id;
        if (userRole === this.USER.ROLE.ADMIN) {
            return true;
        }
        return false;
    }

    isOwner() {
        const user = this.authService.getCurrentUser();
        if (this.article.user_id === user.id) {
            return true;
        }
        return false;
    }
}

export default [
    '$state',
    '$stateParams',
    'ArticlesService',
    'authService',
    'USER',
    ArticleStateController
];
