class ApplicationController {
    constructor($state, authService) {
        this.$state = $state;
        this.authService = authService;
        this.showMenu = false;
        this.authService.setUserFromCookies();
    }

    toggleMenu() {
        this.showMenu = !this.showMenu;
    }

    isState(stateName) {
        if (this.$state.current.name === stateName) {
            return true;
        }
        return false;
    }

    goToState(stateName, params) {
        if (params) {
            this.$state.go(stateName, params);
        } else {
            this.$state.go(stateName);
        }

        this.showMenu = false;
    }

    isUserLoggedIn() {
        return this.authService.isLoggedIn();
    }

    getUserName() {
        if (!this.authService.getCurrentUser()) {
            return undefined;
        }
        return this.authService.getCurrentUser().username;
    }

    getUserId() {
        if (!this.authService.getCurrentUser()) {
            return undefined;
        }
        return this.authService.getCurrentUser().id;
    }

    logout() {
        this.authService.removeCurrentUser();
        this.goToState('app.login');
    }
}

export default [
    '$state',
    'authService',
    ApplicationController
];
