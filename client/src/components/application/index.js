import './stylesheets/application.css!';
import 'babel/external-helpers';
import angular from 'angular';
import 'angular-ui-router';
import 'ui-router-extras';
import ocLazyLoad from 'oclazyload';
import ngLazy from 'angular-lazy';
import 'angular-toastr';
import 'angular-toastr/dist/angular-toastr.min.css!';
import redirectingConfig from './config/redirecting';
import routingConfig from './config/routing';
import errorHandlingConfig from './config/error-handling';
import constants from './config/constants.json!';
import ApplicationController from './application-controller';
import applicationRoute from './application-route';
import AuthService from './services/auth-service';
import UserService from './services/user-service';
import LoggerService from './services/logger';
import httpErrorInterceptorFactory from './config/http-error-interceptor-factory';
import httpErrorInterceptorConfig from './config/http-error-interceptor';
import checkTokenAndLogin from './config/check-token';
import 'angular-cookies';

const dependencies = [
    'ngCookies',
    'ui.router',
    ocLazyLoad,
    'ct.ui.router.extras',
    'ct.ui.router.extras.future',
    ngLazy.name,
    'toastr'
];

const app = angular
    .module('application-component', dependencies)
    .controller('ApplicationController', ApplicationController)
    .config(routingConfig)
    .config(applicationRoute)
    .config(httpErrorInterceptorConfig)
    .run(errorHandlingConfig)
    .run(redirectingConfig)
    .run(checkTokenAndLogin)
    .factory('http-error-interceptor', httpErrorInterceptorFactory)
    .service('authService', AuthService)
    .service('userService', UserService)
    .service('logger', LoggerService);

Object.keys(constants).forEach((constantName) => {
    app.constant(constantName, constants[constantName]);
});

export default app;
