/**
 * Service for interacting with the user entities on the server's database
 */
class UserService {
    constructor(API_URL, $http) {
        this.API_URL = API_URL;
        this.$http = $http;
    }

    /**
     * Returns promise for user object by ID
     *
     * @param {number} id - User's ID
     * @return {Promise} - Promise which returns the user object
     */
    getById(id) {
        return this.$http.get(`${this.API_URL.BASE_API}${this.API_URL.USERS}/${id}`)
            .then(response => response.data);
    }

    /**
     * Returns promise for users array
     *
     * @return {Promise} - Promise which returns the user's list array
     */
    getAll() {
        return this.$http.get(`${this.API_URL.BASE_API}${this.API_URL.USERS}`)
            .then(response => response.data);
    }

    /**
     * Returns promise for updated user by ID and user object
     *
     * @return {Promise} - Promise which returns the updated user object
     */
    update(id, user) {
        return this.$http.put(`${this.API_URL.BASE_API}${this.API_URL.USERS}/${id}`, user);
    }

    /**
     * Returns promise for banned user by ID and user object
     *
     * @return {Promise} - Promise which returns the banned user object
     */
    ban(id, user) {
        return this.$http.put(`${this.API_URL.BASE_API}${this.API_URL.USERS}/${id}`, user);
    }

    /**
     * Gets the list of articles, which are created by current user
     * @param userId
     * @returns the array of articles
     */
    getUserArticles(userId) {
        return this.$http.get(`${this.API_URL.BASE_API}${this.API_URL.ARTICLES}/user/${userId}`)
            .then(response => response.data);
    }
    deleteAllUserArticles(userId) {
        return this.$http.delete(`${this.API_URL.BASE_API}${this.API_URL.ARTICLES}/user/${userId}`)
            .then(res => res.data);
    }
}

export default [
    'API_URL',
    '$http',
    UserService
];
