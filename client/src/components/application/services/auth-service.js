import angular from 'angular';

/**
 * Service for the user, that contains methods for interaction with the user
 */
class AuthService {
    constructor($http, API_URL, USER, $window, $cookies) {
        this.$http = $http;
        this.API_URL = API_URL;
        this.USER = USER;
        this.$window = $window;
        this.$cookies = $cookies;
    }

    showProfile() {
    }

    login(data) {
        return this.$http.post(this.API_URL.BASE + this.API_URL.LOGIN, data)
            .then(response => {
                const userData = response.data;
                this.setCurrentUser(userData);
                return userData;
            })
            .catch(error => error.message);
    }

    register(data) {
        return this.$http.post(this.API_URL.BASE + this.API_URL.REGISTER, data);
    }

    sendPhotoOnServer(file, id) {
        const formData = new FormData();
        formData.append('avatar', file);

        return this.$http.post(`${this.API_URL.BASE_API}${this.API_URL.AVATAR}/${id}`,
            formData, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            });
    }

    setCurrentUser(user) {
        if (!user || !this.$window.localStorage) {
            return;
        }

        this.$http.defaults.headers.common.Authorization = `Bearer ${user.token}`;

        this.$window.localStorage.setItem('user', JSON.stringify(user));
    }

    getCurrentUser() {
        const user = this.$window.localStorage && this.$window.localStorage.getItem('user') || '';
        if (user) {
            return user && JSON.parse(user).user;
        }
        return false;
    }

    setUserFromCookies() {
        const user = this.$cookies.get('user');
        if (user) {
            this.setCurrentUser(JSON.parse(user));
            this.$cookies.remove('user');
        }
    }

    removeCurrentUser() {
        if (!this.$window.localStorage && !this.$window.localStorage.getItem('user')) {
            return;
        }
        this.$window.localStorage.removeItem('user');
    }

    isLoggedIn() {
        if (!this.getCurrentUser()) {
            return false;
        }
        return true;
    }

    isAdmin() {
        if (this.isLoggedIn() && (this.getCurrentUser().role_id === this.USER.ROLE.ADMIN)) {
            return true;
        }
        return false;
    }

    isRegularUser() {
        if (this.isLoggedIn() && (this.getCurrentUser().role_id === this.USER.ROLE.USER)) {
            return true;
        }
        return false;
    }
}

export default [
    '$http',
    'API_URL',
    'USER',
    '$window',
    '$cookies',
    AuthService
];
