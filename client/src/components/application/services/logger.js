/**
 * Provides displaying UI notifications by toasts
 */
class LoggerService {
    constructor(toastr) {
        this.toastr = toastr;
    }

    error(message, title) {
        this.toastr.error(message, title);
    }

    info(message, title) {
        this.toastr.info(message, title);
    }

    success(message, title) {
        this.toastr.success(message, title);
    }

    warning(message, title) {
        this.toastr.warning(message, title);
    }
}

export default [
    'toastr',
    LoggerService
];
