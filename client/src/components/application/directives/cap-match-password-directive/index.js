import angular from 'angular';
import CapMatchPasswordDirectiveController from './cap-match-password-directive-controller';
import capMatchPasswordDirective from './cap-match-password-directive';

const dependencies = [
];

export default angular
    .module('cap-match-password-directive-component', dependencies)
    .controller('CapMatchPasswordDirectiveController', CapMatchPasswordDirectiveController)
    .directive('capMatchPassword', capMatchPasswordDirective);
