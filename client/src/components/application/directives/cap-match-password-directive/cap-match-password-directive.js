import template from './cap-match-password-directive.html!text';

/**
 * Adds HTML with inputs for entering user password. First input is the regular password input field, second - password
 * input field for confirmation re-entered password.
 * It is used as HTML element. It requires ng-model attribute for storing verified password value (type of string).
 * it allows to use min-length attribute in which we can assign minimal password length in symbols. If passwords are
 * equals and their's length is valid, directive returns password string to the Angular's ng-model directive. Otherwise
 * it returns the undefined value.
 */
function capMatchPasswordDirective() {
    return {
        restrict: 'E',
        controller: 'CapMatchPasswordDirectiveController as capMatchPassword',
        template,
        scope: {
            minLength: '='
        },
        require: 'ngModel',
        link: (scope, element, attrs, ngModelCtrl) => {
            if (!ngModelCtrl) {
                return;
            }

            const capMatchPasswordCtrl = scope.capMatchPassword;
            const minLength = scope.minLength;

            if (minLength) {
                capMatchPasswordCtrl.setMinPasswordLength(scope.minLength);
            }

            scope.$watch(() => capMatchPasswordCtrl.getPassword(),
                (newValue) => {
                    ngModelCtrl.$setViewValue(newValue);
                });
        }
    };
}

export default[
    capMatchPasswordDirective
];
