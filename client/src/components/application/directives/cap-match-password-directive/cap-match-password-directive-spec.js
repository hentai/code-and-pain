import angular from 'angular';
import 'angular-mocks';
import component from './index';

describe('Cap Match Password Directive', () => {
    beforeEach(angular.mock.module(component.name));

    let $controller;

    beforeEach(angular.mock.inject((_$controller_) => {
        $controller = _$controller_;
    }));

    describe('Controller Test', () => {
        describe('Password verifying', () => {
            let controller;

            beforeEach(() => {
                controller = $controller('CapMatchPasswordDirectiveController');
            });

            it('Check passwords for equality and min length in 8 chars and return password string if ', () => {
                controller.setMinPasswordLength('8');
                controller.password = 'verifiedpassword';
                controller.passwordConfirm = 'verifiedpassword';
                controller.confirm();
                expect(controller.getPassword()).to.equal('verifiedpassword');
            });

            it('Check passwords for equality and min length in 8 chars and return undefined if they are not equals',
                () => {
                    controller.password = 'verifiedpassword';
                    controller.passwordConfirm = 'anotherpassword';
                    controller.confirm();
                    expect(controller.getPassword()).to.equal(undefined);
                });

            it('Check passwords for equality and min length in 8 chars and return undefined value', () => {
                controller.setMinPasswordLength('8');
                controller.password = 'pass';
                controller.passwordConfirm = 'pass';
                controller.confirm();
                expect(controller.getPassword()).to.equal(undefined);
            });
        });
    });

    describe('Markup Test', () => {
        let $compile;
        let $rootScope;
        let directiveElement;

        beforeEach(angular.mock.inject((_$compile_, _$rootScope_) => {
            $compile = _$compile_;
            $rootScope = _$rootScope_;
            directiveElement = $compile('<cap-match-password ng-model="var"></cap-match-password>')($rootScope);
        }));

        it('Chek if first span with icon is hidden', () => {
            $rootScope.$digest();
            const spanElement = angular.element(directiveElement.find('span')[0]);
            expect(spanElement.hasClass('ng-hide')).to.equal(true);
        });

        it('Chek if second span with icon is hidden', () => {
            $rootScope.$digest();
            const spanElement = angular.element(directiveElement.find('span')[1]);
            expect(spanElement.hasClass('ng-hide')).to.equal(true);
        });
    });
});
