class CapMatchPasswordDirectiveController {
    constructor() {
        this.password = '';
        this.passwordConfirm = '';
        this.passwordVerified = '';
        this.passwordLengthValid = false;
        this.minPasswordLength = 0;
    }

    /**
     * Checks passwords for matching. If password's length is valid and both passwords are equals, saves password into
     * this.passwordVerified property.
     */
    confirm() {
        if (this.password.length >= this.minPasswordLength) {
            this.passwordLengthValid = true;

            if (this.password === this.passwordConfirm) {
                this.passwordVerified = this.password;
            } else {
                this.passwordVerified = undefined;
            }
        } else {
            this.passwordLengthValid = false;
            this.passwordVerified = undefined;
        }
    }

    /**
     * Saves minimal password length into this.minPasswordLength property
     *
     * @param minPasswordLength {string} - Minimal password length defined in the min-length attribute
     */
    setMinPasswordLength(minPasswordLength) {
        this.minPasswordLength = parseInt(minPasswordLength, 10);
    }

    /**
     * Returns verified password stored in the this.passwordVerified property. If compared passwords are equals,
     * returns verified password, othervice returns undefined.
     *
     * @return {string|undefined} - Verified password
     */
    getPassword() {
        return this.passwordVerified;
    }
}

export default [
    CapMatchPasswordDirectiveController
];
