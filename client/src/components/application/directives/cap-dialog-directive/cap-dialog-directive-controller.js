class CapDialogDirectiveController {
    /**
     * @prop {boolean} this.show - flag which is set to show and hide the dialog. Use true value to show the dialog
     * and false to hide it.
     * @prop {string} this.title - dialog's title
     */
    constructor() {
        this.init();
    }

    init() {
        if (!this.title) {
            this.title = 'Info';
        }
    }

    closeDialog() {
        this.show = false;
    }
}

export default [
    CapDialogDirectiveController
];
