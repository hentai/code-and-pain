import template from './cap-dialog.html!text';

function capDialogDirective() {
    return {
        restrict: 'E',
        controller: 'CapDialogDirectiveController as $ctrl',
        template,
        scope: { },
        bindToController: {
            show: '=',
            title: '@dialogTitle'
        },
        transclude: true
    };
}

export default[
    capDialogDirective
];
