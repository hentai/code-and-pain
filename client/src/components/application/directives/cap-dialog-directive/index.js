import './cap-dialog-directive.css!';
import angular from 'angular';
import CapDialogDirectiveController from './cap-dialog-directive-controller';
import capDialogDirective from './cap-dialog-directive';

const dependencies = [

];

export default angular
    .module('cap-dialog-directive-component', dependencies)
    .controller('CapDialogDirectiveController', CapDialogDirectiveController)
    .directive('capDialog', capDialogDirective);
