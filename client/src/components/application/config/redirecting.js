function redirectingConfig($rootScope, $state, authService) {
    $rootScope.$on('$stateChangeStart', (event, toState, toParams) => {
        if (toState.redirectTo) {
            event.preventDefault();
            $state.go(toState.redirectTo, toParams);
        } else if (toState.forbidden && !authService.isAdmin()) {
            event.preventDefault();
            $state.go('app.index', toParams);
        }
    });
}

export default [
    '$rootScope',
    '$state',
    'authService',
    redirectingConfig
];
