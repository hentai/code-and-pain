import angular from 'angular';

function httpErrorInterceptorConfig($httpProvider, toastrConfig) {
    $httpProvider.interceptors.push('http-error-interceptor');

    angular.extend(toastrConfig, {
        positionClass: 'toast-bottom-right'
    });
}

export default [
    '$httpProvider',
    'toastrConfig',
    httpErrorInterceptorConfig
];
