function httpErrorInterceptorFactory($q, logger) {
    return {
        'responseError': (rejection) => {
            let message;

            if (rejection.status === -1) {
                message = 'Server is not available';
            } else if (rejection.config.method === 'HEAD') {
                return $q.reject(rejection);
            } else if (rejection.data && rejection.data.message) {
                message = rejection.data.message;
            } else {
                message = rejection.data;
            }

            let title = `${rejection.status} (${rejection.statusText})`;

            if (rejection.status === 401) {
                title = 'Unauthorized, please sign in!';
                message = 'Permission denied';
            }

            logger.error(message, title);

            return $q.reject(rejection);
        }
    };
}

export default [
    '$q',
    'logger',
    httpErrorInterceptorFactory
];
