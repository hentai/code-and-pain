/**
 * User model
 * Class that represents a user
 */
class User {
    constructor(user) {
        this.username = user.username;
        this.email = user.email;
        this.registrationDate = user.registrationDate;
        this.description = user.description;
        this.avatar = user.avatar;
    }
}

export default [
    User
];
