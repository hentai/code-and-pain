/**
 * Service for the code-editor, that contains methods for interaction with the snippers
 */
class SnapshotService {
    constructor($http, API_URL, $window, $stateParams, logger) {
        this.$http = $http;
        this.API_URL = API_URL;
        this.$window = $window;
        this.$stateParams = $stateParams;
        this.logger = logger;
        this.snapshots = [];
    }

    saveCode(code, title) {
        const user = this.getCurrentUser() || {id: null};
        const data = {
            title,
            code,
            user_id: user.id
        };
        if (this.$stateParams.snapshot === undefined) {
            return this.postCode(data);
        }
        return this.updateCode(data);
    }

    postCode(data) {
        return this.$http.post(`${this.API_URL.BASE_API}${this.API_URL.SNAPSHOTS}`, data)
            .then(response => {
                this.logger.success('save');
                this.getSnapshots();
                return response.data;
            });
    }

    updateCode(data) {
        return this.$http.put(`${this.API_URL.BASE_API}${this.API_URL.SNAPSHOTS}/${this.$stateParams.SNAPSHOTS}`, data)
            .then(response => {
                this.logger.success('update');
                return response.data;
            });
    }

    getSnapshotById(id) {
        return this.$http.get(`${this.API_URL.BASE_API}${this.API_URL.SNAPSHOTS}/${id}`)
            .then(response => response.data);
    }

    getCurrentUser() {
        const user = this.$window.localStorage && this.$window.localStorage.getItem('user');
        return user && JSON.parse(user).user;
    }

    getSnapshots() {
        return this.$http.get(this.API_URL.BASE_API + this.API_URL.SNAPSHOTS)
            .then(response => {
                this.snapshots = response.data;
                return this.snapshots;
            });
    }

}

export default [
    '$http',
    'API_URL',
    '$window',
    '$stateParams',
    'logger',
    SnapshotService
];
