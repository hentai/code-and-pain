/**
 * Service for the code-editor, that contains methods for interaction with the snippers
 */
class SnippetService {
    constructor($http, API_URL, $window, $stateParams, logger) {
        this.$http = $http;
        this.API_URL = API_URL;
        this.$window = $window;
        this.$stateParams = $stateParams;
        this.logger = logger;
    }

    saveCode(code, title) {
        const user = this.getCurrentUser() || {id: null};
        const data = {
            title,
            code,
            user_id: user.id
        };
        if (this.$stateParams.snippet === undefined) {
            return this.postCode(data);
        }
        return this.updateCode(data);
    }

    postCode(data) {
        return this.$http.post(`${this.API_URL.BASE_API}${this.API_URL.SNIPPETS}`, data)
            .then(response => {
                this.logger.success('save');
                return response.data;
            });
    }

    updateCode(data) {
        return this.$http.put(`${this.API_URL.BASE_API}${this.API_URL.SNIPPETS}/${this.$stateParams.snippet}`, data)
            .then(response => {
                this.logger.success('update');
                return response.data;
            });
    }

    getSnippetById(id) {
        return this.$http.get(`${this.API_URL.BASE_API}${this.API_URL.SNIPPETS}/${id}`)
            .then(response => response.data);
    }

    getCurrentUser() {
        const user = this.$window.localStorage && this.$window.localStorage.getItem('user');
        return user && JSON.parse(user).user;
    }

    getSnippets() {
        return this.$http.get(this.API_URL.BASE_API + this.API_URL.SNIPPETS)
            .then(response => response.data);
    }

}

export default [
    '$http',
    'API_URL',
    '$window',
    '$stateParams',
    'logger',
    SnippetService
];
