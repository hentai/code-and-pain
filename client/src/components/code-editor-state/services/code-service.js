/**
 * Contains methods for compilation code on server and getting of results
 */
class CodeService {

    constructor($http, API_URL) {
        this.$http = $http;
        this.API_URL = API_URL;
    }

    /**
     * Sends code for compilation to server and gets compiled code
     * All symbols " are escaped with regular expressions to not conflict with JSON
     * @param inputCode code from code editor
     * @returns response.data JSON object with field code, which contains
     * value of compiled code
     */
    compileCode(inputCode) {
        const data = {
            code: inputCode
        };
        return this.$http.post(this.API_URL.BASE_API + this.API_URL.COMPILE, data)
            .then(response => response.data)
            .catch(error => error.data);
    }

}

export default [
    '$http',
    'API_URL',
    CodeService
];
