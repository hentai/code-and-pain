import template from './code-editor-state.html!text';

function codeEditorRouteConfig($stateProvider) {
    $stateProvider
        .state('app.code-editor', {
            url: 'code-editor?snippet',
            views: {
                application: {
                    controller: 'CodeEditorStateController as codeEditorState',
                    template
                }
            }
        });
}

export default [
    '$stateProvider',
    codeEditorRouteConfig
];
