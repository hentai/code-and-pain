import template from './cap-console-component.html!text';

export default {
    bindings: {
        compiledCode: '<'
    },
    bindToController: true,
    controller: 'CapConsoleComponentController',
    require: { },
    template
};
