import './code-editor-state.css!';
import angular from 'angular';
import 'angular-ui-router';
import CodeEditorStateController from './code-editor-state-controller';
import codeEditorRouteConfig from './code-editor-route';
import CodeService from './services/code-service';
import SnippetService from './services/snippet-service';
import SnapshotService from './services/snapshot-service';
import 'components/code-editor-state/cap-code-editor/index';
import 'components/code-editor-state/cap-console/index';
import capDialogDirective from 'components/application/directives/cap-dialog-directive/index';
import capSnippetsList from './cap-snippets-list/index';

const dependencies = [
    'ui.router',
    'cap-code-editor-component',
    'cap-console-component',
    capSnippetsList.name,
    capDialogDirective.name
];

export default angular
    .module('code-editor-state-component', dependencies)
    .controller('CodeEditorStateController', CodeEditorStateController)
    .service('SnapshotService', SnapshotService)
    .service('CodeService', CodeService)
    .service('SnippetService', SnippetService)
    .config(codeEditorRouteConfig);
