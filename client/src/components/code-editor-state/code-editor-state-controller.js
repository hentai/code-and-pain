import angular from 'angular';

class CodeEditorStateController {
    constructor(CodeService, SnippetService, SnapshotService, $scope, $stateParams, $state) {
        this.CodeService = CodeService;
        this.SnippetService = SnippetService;
        this.SnapshotService = SnapshotService;
        this.$stateParams = $stateParams;
        this.$state = $state;
        this.$scope = $scope;
        this.inputCode = '';
        this.compiledCode = '';
        this.page = 'view1';
        this.currentViewMode = 'compiled';
        this.show = true;
        this.snippetShortCut = '';
        this.snippetCode = '';
        this.textFile = null;
        this.compilationResult = '';
        this.isDialogShown = false;
        this.isSaveDialogShown = false;
        this.isSnippetDialogShown = false;
        this.isSnapshotDialogShown = false;
        this.currentSnippetTitle = '';
        this.addSnippetMode = false;
        this.getSnippet();
    }

    getSnippet() {
        if (this.$stateParams.snippet) {
            this.SnippetService.getSnippetById(this.$stateParams.snippet)
                .then(response => {
                    if (response) {
                        this.inputCode = response.code;
                        this.$scope.$broadcast('setDataFromBackEnd');
                    } else {
                        this.$state.go('app.code-editor', {snippet: null});
                    }
                });
        }
    }

    showChoiseButtons() {
        this.show = true;
    }

    hideChoiseButtons() {
        this.show = false;
    }

    setInputCode(inputCode) {
        this.inputCode = inputCode;
    }

    saveCode() {
        this.SnippetService.saveCode(this.inputCode, this.currentSnippetTitle)
            .then(response => {
                if (response) {
                    this.$state.go('app.code-editor', {snippet: response.id});
                }
            });
    }

    showInfoDialog() {
        this.isDialogShown = true;
    }

    showSaveDialog() {
        this.isSaveDialogShown = true;
    }

    showSnippetDialog() {
        this.isSnippetDialogShown = true;
    }

    showSnapshotDialog() {
        this.isSnapshotDialogShown = true;
    }

    /**
     * Sends code for compilation to server and gets compiled code
     * @param inputCode code from code editor
     * @returns compiledCode code compiled on server
     */
    compileCode() {
        return this.CodeService.compileCode(this.inputCode)
            .then(response => {
                if (response.error) {
                    this.errorHandling(response.error);
                } else {
                    this.compiledCode = response.code;
                    this.initTextFile(this.compiledCode);
                }
                return this.compiledCode;
            });
    }

    initTextFile(code) {
        const consoleRedef = `console.log = (${this.consoleRedefinition.toString()})();`;
        const result = 'self.postMessage(console.log.getStorage().join("\\n"));';
        const data = new Blob([consoleRedef, code, result], {type: 'text/plain'});
        this.textFile = window.URL.createObjectURL(data);
        this.startWorker();
    }

    consoleRedefinition() {
        const storage = [];
        /* eslint-disable no-console */
        const originConsole = console.log;
        /* eslint-enable no-console */
        const log = function (...arg) {
            storage.push(arg[0]);
            originConsole.apply(console, arg);
        };
        log.getStorage = () => storage;
        return log;
    }

    startWorker() {
        this.codeWorker = new Worker(this.textFile);
        this.codeWorker.onmessage = this.handleMessageFromWorker.bind(this);
        this.codeWorker.postMessage('start');
    }

    handleMessageFromWorker(msg) {
        this.compilationResult = msg.data;
        this.$scope.$apply();
    }

    addSnippet() {
        this.addSnippetMode = true;
    }

    saveSnippet() {
        this.addSnippetMode = false;
        this.SnapshotService.saveCode(this.snippetCode, this.snippetShortCut);
    }

    errorHandling(errorsArray) {
        const errors = errorsArray.split('\n');
        const editor = document.getElementsByClassName('CodeMirror');
        const doc = editor[0].CodeMirror.doc;
        for (let i = 0; i < errors.length; i++) {
            const currentLine = errors[i].match(/\d+/g)[0];
            const line = doc.getLineHandle(currentLine);
            doc.addLineClass(line, 'gutter', 'line-error');

            const errorTip = document.getElementsByClassName('CodeMirror-gutter-elt')[currentLine];

            errorTip.onmouseover = () => {
                angular.element(errorTip).append(`<div class="error-information">${errors[i]}</div>`);
            };
            errorTip.onmouseout = () => {
                document.getElementsByClassName('error-information')[0].remove();
            };
        }
    }

    clearCode() {
        this.$scope.$broadcast('clear');
    }
}

export default [
    'CodeService',
    'SnippetService',
    'SnapshotService',
    '$scope',
    '$stateParams',
    '$state',
    CodeEditorStateController
];
