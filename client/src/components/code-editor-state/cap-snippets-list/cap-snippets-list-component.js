import template from './cap-snippets-list-component.html!text';

export default {
    bindings: { },
    bindToController: true,
    controller: 'CapSnippetsListComponentController',
    require: { },
    template
};
