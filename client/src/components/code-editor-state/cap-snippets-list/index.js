import './cap-snippets-list-component.css!';
import angular from 'angular';
import 'angular-ui-router';
import CapSnippetsListComponentController from './cap-snippets-list-component-controller';
import capSnippetsListComponent from './cap-snippets-list-component';

const dependencies = [
    'ui.router'
];

export default angular
    .module('cap-snippets-list-component', dependencies)
    .controller('CapSnippetsListComponentController', CapSnippetsListComponentController)
    .component('capSnippetsList', capSnippetsListComponent);
