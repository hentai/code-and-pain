class CapSnippetsListComponentController {
    constructor(SnippetService, $state) {
        this.SnippetService = SnippetService;
        this.snippets = [];
        this.getSnippets();
        this.$state = $state;
    }

    getSnippets() {
        this.SnippetService.getSnippets().then(data => {
            this.snippets = data;
            return this.snippets;
        });
    }

    chooseSnippet(snippet) {
        this.$state.go('app.code-editor', {snippet: snippet.id});
    }
}

export default [
    'SnippetService',
    '$state',
    CapSnippetsListComponentController
];
