import CodeMirror from 'codemirror';

function createDirective(SnapshotService) {
    return {
        scope: false,
        link: (scope, element) => {
            const textEditor = CodeMirror.fromTextArea(element[0], scope.$ctrl.getCodemirrorConfig());
            const currentCode = scope.$ctrl.$window.localStorage.getItem('currentCode') || '';
            textEditor.setValue(currentCode);
            scope.$ctrl.inputCode = currentCode;

            SnapshotService.getSnapshots();

            if (scope.$ctrl.inputCode) {
                textEditor.setValue(scope.$ctrl.inputCode);
            }

            function addSnippet(lineNumber) {
                const code = textEditor.doc.getLine(lineNumber);
                const snippets = SnapshotService.snapshots;
                snippets.forEach((snippet) => {
                    if (snippet.name === code) {
                        textEditor.doc.replaceRange(snippet.code, {line: lineNumber, ch: code},
                            {line: lineNumber + 1});
                        textEditor.doc.setCursor({line: textEditor.doc.lastLine()});
                    }
                });
            }

            textEditor.setOption('extraKeys', {
                Tab: (cm) => {
                    addSnippet(cm.getCursor().line);
                }
            });

            /**
             * Helps us add snippet code from server
             */
            scope.$on('setDataFromBackEnd', (value) => {
                const code = value.targetScope.codeEditorState.inputCode;
                textEditor.setValue(code);
            });

            scope.$on('clear', () => {
                textEditor.setValue('');
            });

            /**
             * Adds listener for user inputText
             */
            function addChangeListener() {
                let inputCode = '';

                textEditor.on('change', () => {
                    inputCode = textEditor.getValue();
                    scope.$ctrl.setInputCode(inputCode);
                    scope.$ctrl.$window.localStorage.setItem('currentCode', inputCode);
                });
            }

            addChangeListener();

            function addChangeEditorThemeListener() {
                const lightButton = document.getElementsByClassName('light-theme-button')[0];
                const darkButton = document.getElementsByClassName('dark-theme-button')[0];

                lightButton.onclick = () => {
                    textEditor.setOption('theme', 'default');
                };

                darkButton.onclick = () => {
                    textEditor.setOption('theme', 'dracula');
                };
            }

            addChangeEditorThemeListener();
        }
    };
}

export default [
    'SnapshotService',
    createDirective
];
