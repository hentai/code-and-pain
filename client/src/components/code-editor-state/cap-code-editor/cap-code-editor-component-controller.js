import 'codemirror/theme/dracula.css!';

class CapCodeEditorComponentController {

    constructor($window) {
        this.$window = $window;
    }

    setInputCode(inputCode) {
        this.onSetInputCode({inputCode});
    }

    getInputCode() {
        return this.inputCode;
    }

    /**
     * Gets CodeMirror configs to render CodeAndPain editor.
     * @returns config config object
     */
    getCodemirrorConfig() {
        const config = {
            mode: {
                name: 'javascript',
                json: true
            },
            extraKeys: {
                'Ctrl-Scape': 'autocomplete',
                F11: (cm) =>
                    cm.setOption('fullScreen', !cm.getOption('fullScreen')),
                Esc: (cm) => {
                    if (cm.getOption('fullScreen')) {
                        cm.setOption('fullScreen', false);
                    }
                },
                'Ctrl-Enter': () => {
                    this.onCompileCode();
                }
            },
            theme: 'default',
            autofocus: true,
            lineNumbers: true
        };

        return config;
    }

}

export default [
    '$window',
    CapCodeEditorComponentController
];
