import './about-state.css!';
import angular from 'angular';
import 'angular-ui-router';
import AboutStateController from './about-state-controller';
import aboutRouteConfig from './about-route';

const dependencies = [
    'ui.router'
];

export default angular
    .module('about-state-component', dependencies)
    .controller('AboutStateController', AboutStateController)
    .config(aboutRouteConfig);
