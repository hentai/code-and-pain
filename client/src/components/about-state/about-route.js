import template from './about-state.html!text';

function aboutRouteConfig($stateProvider) {
    $stateProvider
        .state('app.about', {
            url: 'about',
            views: {
                application: {
                    controller: 'AboutStateController as aboutState',
                    template
                }
            }
        });
}

export default [
    '$stateProvider',
    aboutRouteConfig
];
