class LoginStateController {
    constructor(authService, $state) {
        this.authService = authService;
        this.$state = $state;
        this.redirectIfLoggedIn();
    }

    isLoggedIn() {
        return this.authService.isLoggedIn();
    }

    redirectIfLoggedIn() {
        if (this.isLoggedIn()) {
            this.$state.go('app.code-editor');
        }
    }
}

export default [
    'authService',
    '$state',
    LoginStateController
];
