import './cap-login-component.css!';
import angular from 'angular';
import CapLoginComponentController from './cap-login-component-controller';
import capLoginComponent from './cap-login-component';

const dependencies = [
];

export default angular
    .module('cap-login-component', dependencies)
    .controller('CapLoginComponentController', CapLoginComponentController)
    .component('capLogin', capLoginComponent);
