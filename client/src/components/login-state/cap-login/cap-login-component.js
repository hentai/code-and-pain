import template from './cap-login-component.html!text';

export default {
    bindings: { },
    bindToController: true,
    controller: 'CapLoginComponentController',
    require: { },
    template
};
