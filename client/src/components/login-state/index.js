import './login-state.css!';
import angular from 'angular';
import 'angular-ui-router';
import LoginStateController from './login-state-controller';
import loginRouteConfig from './login-route';
import loginComponent from './cap-login/index';

const dependencies = [
    'ui.router',
    loginComponent.name
];

export default angular
    .module('login-state-component', dependencies)
    .controller('LoginStateController', LoginStateController)
    .config(loginRouteConfig);
