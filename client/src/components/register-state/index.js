import './register-state.css!';
import angular from 'angular';
import 'angular-ui-router';
import RegisterStateController from './register-state-controller';
import registerRouteConfig from './register-route';
import capMatchPasswordDirective from './../application/directives/cap-match-password-directive/index';

const dependencies = [
    'ui.router',
    capMatchPasswordDirective.name
];

export default angular
    .module('register-state-component', dependencies)
    .controller('RegisterStateController', RegisterStateController)
    .config(registerRouteConfig);
