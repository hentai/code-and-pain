class RegisterStateController {
    constructor(authService, $state, API_URL) {
        this.authService = authService;
        this.$state = $state;
        this.API_URL = API_URL;
    }

    registerSubmit() {
        this.authService.register(this.fields)
            .then(response => {
                if (response.status === 200) {
                    this.$state.go('app.login');
                }
            });
    }

}

export default [
    'authService',
    '$state',
    'API_URL',
    RegisterStateController
];
