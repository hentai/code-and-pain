import './profile-state.css!';
import angular from 'angular';
import 'angular-ui-router';
import ProfileStateController from './profile-state-controller';
import profileRouteConfig from './profile-route';
import 'angular-xeditable';
import 'angular-xeditable/dist/css/xeditable.css!';
import capUploadPhotoDirective from './cap-upload-photo-directive';
// import capArticlesList from './../../blog/cap-articles-list/index';

const dependencies = [
    'ui.router',
    'xeditable'
];

export default angular
    .module('profile-state-component', dependencies)
    .controller('ProfileStateController', ProfileStateController)
    .directive('capUploadPhotoDirective', capUploadPhotoDirective)
    .config(profileRouteConfig);
