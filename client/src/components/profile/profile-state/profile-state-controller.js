class ProfileStateController {
    constructor(USER, $stateParams, $state, authService, userService, logger) {
        this.USER = USER;
        this.$stateParams = $stateParams;
        this.authService = authService;
        this.userService = userService;
        this.$state = $state;
        this.logger = logger;
        this.profileId = $stateParams.id;
        this.user = {};
        this.isAdmin = false;
        this.isOwner = false;
        this.getUserById(this.profileId);
        this.userArticles = [];
        this.getUserArticles();
    }

    sendPhotoOnServer(file) {
        this.authService.sendPhotoOnServer(file, this.profileId).then((response) => {
            this.user.avatar = response.data.data;
        });
    }

    getUserById(id) {
        const userPromise = this.userService.getById(id);
        userPromise.then((user) => {
            this.user = user;
            this.checkRole();
        }, () => {
            this.$state.go('app.code-editor');
        });
    }

    checkRole() {
        const userRole = this.authService.getCurrentUser();
        if (userRole.role_id === this.USER.ROLE.ADMIN) {
            this.isAdmin = true;
        }

        if (userRole.role_id === this.USER.ROLE.USER) {
            if (userRole.email === this.user.email) {
                this.isOwner = true;
            }
        }
    }

    saveUser() {
        // TODO: update token with new user data
        const userPromise = this.userService.update(this.profileId, this.user);
        userPromise.then((response) => {
            this.user = response.data;
        });
    }

    /**
     * Sends user object to the server for update
     *
     * @param {Object} user - An user object
     * @param {Boolean} banStatus - ban or unban for user
     */
    banUser(user, banStatus) {
        user.isBanned = banStatus;
        this.userService.update(user.id, user)
            .then(() => {
                this.logger.success('User\'s profile successfully updated', 'Success');
            })
            .catch((error) => {
                this.logger.error(error, 'Error');
            });
    }

    /**
     * Gets list of articles, which are created by current user
     */
    getUserArticles() {
        this.userService.getUserArticles(this.profileId).then(data => {
            this.userArticles = data;
        });
    }
    deleteAllUserArticles() {
        this.userService.deleteAllUserArticles(this.profileId)
            .then(() => {
                this.logger.success('All articles deleted!');
            })
            .catch(error => {
                this.logger.error(error, 'Error');
            });
    }
}

export default [
    'USER',
    '$stateParams',
    '$state',
    'authService',
    'userService',
    'logger',
    ProfileStateController
];
