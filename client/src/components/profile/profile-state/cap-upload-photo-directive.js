function createDirective() {
    return {
        link: (scope, element) => {
            element.bind('change', () => {
                const file = element[0].files[0];
                scope.profileState.sendPhotoOnServer(file);
            });
        }
    };
}


export default [
    createDirective
];
